//
//  ViewController.swift
//  patternUnlock
//
//  Created by Admin on 3/18/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class PatternLockViewController: UIViewController, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var PatternLockCollection: UICollectionView!
    @IBOutlet weak var containView: UIView!
    @IBOutlet weak var patternView: UIView!
    @IBOutlet weak var resetButton: UIButton!
    @IBOutlet weak var confirmPassword: UIButton!
    let animateView = UIView()

    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    
    let contentView = UIView()
    var countKey = 0
    var heightKeyView: CGFloat = 0
    var widthKeyView: CGFloat = 0
    let spacingKeyView: CGFloat = 0
    let maxKey = 10
    let minKey = 4
    
    let keycode = [0: "0", 1: "1", 2:"2", 3:"3", 4:"4",
                      5: "5", 6:"6", 7: "7", 8:"8", 9:"9",
                      10:"iconThemeSportsBadminton.png",
                      11:"iconThemeSportsBaseball.png",
                      12:"iconThemeSportsBasketball.png",
                      13: "iconThemeSportsBicycle.png",
                      14: "iconThemeSportsSwimming.png",
                      15: "iconThemeSportsBowling.png",
                      16: "iconThemeSportsFootball.png",
                      17: "iconThemeSportsGolf.png",
                      18: "iconThemeSportsPingpong.png",
                      19: "iconThemeSportsSki.png"]
    var imageKey = UIImageView()
    
    let column:CGFloat = 5
    let row:CGFloat = 4
    var offset:CGFloat = 9
    var edge: CGFloat = 50
    var edgeTop: CGFloat = 13
    var edgeBottom: CGFloat = 12
    var edgeLeft: CGFloat = 45
    var edgeRight: CGFloat = 44
    var distance2Center: CGFloat = 0
    
    var numberTempRandom = [Int : String]()
    var startPoint = CGPoint()
    var endPoint = CGPoint()
    var drawingLine = LineView()
    var drawedLine = LineView()
    var TrackPath = [(CGPoint, CGPoint)]()
    
    let imageCircleView = UIImageView()
    struct Circle {
        var key: [Int: String]
        var centerPoint: CGPoint
    }
    var circleArr = [Circle]()
    var index_circle = 0
    let viewDraw = UIView()
    
    struct PasswordItem{
        var positionOfCircleArr: Int
        var key: [Int: String]
    }
    var notchecking = true
    var correctPassword = false
    var emptyPassword = true
    var password = [PasswordItem]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        numberTempRandom = keycode

        self.PatternLockCollection.delegate = self
        self.PatternLockCollection.dataSource = self
        PatternLockCollection.register(UINib(nibName: "PatternLockCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "PatternLockCell")
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        ResetStatePatternLock()
        setupButton(button: resetButton)
        setupButton(button: confirmPassword)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        setupLayoutCollectionView()
        setupViewContainsDraw()
        
        setupInitializedValue()
        setupSwipeAction()
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.setupCircle()
        }
        
    }

    func setupInitializedValue(){
        resetButton.isEnabled = true
        resetButton.alpha = 1
        password = []
        countKey = 0
        emptyPassword = true
        correctPassword = false
        notchecking = true
        distance2Center = edge + offset
    }

    func setupButton(button: UIButton){
        let marginWidthPattern = edge * column + offset * (column - 1) + edgeLeft + edgeRight
        let k = PatternLockCollection.bounds.width / marginWidthPattern

        let layer = CAGradientLayer()
        let color1 = UIColor(red: 88/255, green: 222/255, blue: 252/255, alpha: 1).cgColor
        let color2 = UIColor(red: 92/255, green: 130/255, blue: 253/255, alpha: 1).cgColor
        let color3 = UIColor(red: 118/255, green: 51/255, blue: 253/255, alpha: 1).cgColor
        
        layer.frame = CGRect(x: 0, y: 0, width: 400, height: 70) //button.bounds
        layer.colors = [color1, color2, color3]
        layer.startPoint = CGPoint(x: 0, y: 0)
        layer.endPoint = CGPoint(x: 1, y: 0)
        layer.locations = [0.0, 0.5, 1.0]
        button.layer.cornerRadius = button.bounds.height/2
        button.layer.insertSublayer(layer, at: 0)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 17 * k)
        button.clipsToBounds = true
    }
    
    func setupLayoutCollectionView(){
        let marginWidthPattern = edge * column + offset * (column - 1) + edgeLeft + edgeRight
        let k = PatternLockCollection.bounds.width / marginWidthPattern
        edge *= k
        offset *= k
        edgeTop *= k
        edgeBottom *= k
        edgeLeft *= k
        edgeRight *= k
        
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: edge, height: edge)
        layout.minimumInteritemSpacing = offset
        layout.minimumLineSpacing = offset
        layout.sectionInset = UIEdgeInsets(top: edgeTop, left: edgeLeft, bottom: edgeBottom, right: edgeRight)
        PatternLockCollection.collectionViewLayout = layout
    }

    func setupViewContainsDraw(){
        let background = UIImageView(image: UIImage(named: "background_image.png"))
        background.frame = CGRect(x: 0, y: 0, width: patternView.bounds.width + 100, height: patternView.bounds.height + 100)
        patternView.clipsToBounds = true
        patternView.insertSubview(background, belowSubview: PatternLockCollection)//subview 0
        
        PatternLockCollection.backgroundColor = UIColor.clear
        animateView.frame = CGRect(x: 0, y: 0, width: self.patternView.bounds.width/10, height: self.patternView.bounds.width/10)
        animateView.layer.cornerRadius = self.PatternLockCollection.bounds.width/20
        animateView.center = CGPoint(x: patternView.frame.width/2, y: patternView.frame.height/2)
        animateView.backgroundColor = UIColor(red: 0, green: 0.455, blue: 0.756, alpha: 0.5)
        patternView.insertSubview(animateView, belowSubview: PatternLockCollection)//subview 1
        
        viewDraw.frame = PatternLockCollection.bounds
        viewDraw.backgroundColor = .clear
        PatternLockCollection.addSubview(viewDraw)
        
        UIView.animate(withDuration: 6, animations: {
            self.animateView.transform = CGAffineTransform(scaleX: 16, y: 10)
            self.animateView.alpha = 0.7
        })
    }
    
    func setupCircle(){
        let circleView = UIView()
        circleView.frame = patternView.bounds
        circleView.center = CGPoint(x: patternView.frame.width/2, y: patternView.frame.height/2)
        circleView.backgroundColor = .clear
        PatternLockCollection.insertSubview(circleView, belowSubview: viewDraw) //subview 3
        
        //add circle
        for (position, circle) in circleArr.enumerated(){
            let imageCircle = UIImageView(frame: CGRect(x:  circle.centerPoint.x - 6, y: circle.centerPoint.y - 6, width: 12, height: 12))
            imageCircle.alpha = 0
            switch position{
            case 0, 4, 15, 19:
                imageCircle.image = UIImage(named: "circle.png")
            case 1, 2, 3, 5, 6, 7, 9, 11:
                imageCircle.image = UIImage(named: "circle_red.png")
            default:
                imageCircle.image = UIImage(named: "circle_blue.png")
            }
            circleView.insertSubview(imageCircle, at: position)
        }
        
        //add animation for circle
        for (position,subview) in circleView.subviews.enumerated(){
            switch position{
            case 7, 12:
                UIView.animate(withDuration: 0.5) {
                    subview.alpha = 1
                }
            case 6,8,11,13:
                UIView.animate(withDuration: 1, delay: 0.5, options: .curveLinear, animations: {
                    subview.alpha = 1
                }, completion: nil)
            default:
                UIView.animate(withDuration: 2, delay: 1.5, options: .curveLinear, animations: {
                    subview.alpha = 1
                }, completion: nil)
            }
        }
    }
    
    func setupGesture(){
        let pan = UIPanGestureRecognizer(target: self, action: #selector(handleGesture))
        self.viewDraw.addGestureRecognizer(pan)
    }
    
    func selectedKey(position point: CGPoint, isRedaw: Bool){
        let imageCircleView = UIImageView(image: UIImage(named: "circle.png"))
        imageCircleView.frame = isRedaw ? CGRect(x:  point.x - 6, y: point.y - 6, width: 12, height: 12) : CGRect(x:  point.x - 4, y: point.y - 4, width: 8, height: 8)
        viewDraw.addSubview(imageCircleView)
        
        UIView.animate(withDuration: 0.5, animations: {
            imageCircleView.transform = CGAffineTransform(scaleX: 2, y: 2)
        }) { (finished) in
            imageCircleView.transform = CGAffineTransform.identity
        }
    }
    
    func drawLine(line: inout LineView, startPoint: CGPoint, endPoint: CGPoint, color: CGColor?){
        line = LineView(frame: self.PatternLockCollection.bounds)
        line.setPoints(fromPoint: startPoint, toPoint: endPoint)
        if let color = color{
            line.setColorLine(color: color)
        }
        viewDraw.addSubview(line)
    }
    
    func isInsideKey(firstCell: Bool, centerCell: CGPoint, point: CGPoint) -> Bool{
        let distance = sqrt(pow(point.x - centerCell.x, 2) + pow(point.y - centerCell.y, 2))
        let radiusTemp = firstCell ? edge : edge/3
        return distance <= radiusTemp ? true : false
    }
    
    @IBAction func clickSegmentControl(_ sender: Any) {
        switch segmentControl.selectedSegmentIndex{
        case 0: //Tap
            resetState(loadView: true)
            animateView.transform = .identity
            viewDraw.isHidden = true
            animateView.isHidden = true
        case 1: //Swipe
            setupSwipeAction()
        default:
            break
        }
    }
    
    func setupSwipeAction(){
        viewDraw.isHidden = false
        self.resetState(loadView: true)
        setupGesture()
    }
    
    @objc func handleGesture(gesture: UIPanGestureRecognizer){
        self.viewDraw.isHidden = false
        switch gesture.state {
        case .began:
            beginGesture(gesture: gesture)
        case .changed:
            changeGesture(gesture: gesture)
        case .ended:
            endGesture(gesture: gesture)
        default:
            break
        }
    }
    
    func addKey(value: String){
        widthKeyView = (self.containView.bounds.width - CGFloat(maxKey) * spacingKeyView) / CGFloat(maxKey) - CGFloat(5)
        heightKeyView = widthKeyView
        
        let subView = UIView(frame: CGRect(x: self.view.frame.width + 10,y : self.contentView.frame.origin.y, width: widthKeyView, height: heightKeyView))
        let image = UIImageView(image: UIImage(named: "box_progress_pushed-password.png"))
        image.contentMode = .scaleAspectFit
        subView.addSubview(image)
        loadKey(key: subView, value: value)
        
        contentView.center = CGPoint(x: self.containView.frame.width/2, y: self.containView.frame.height/2)
        if countKey == 0{
            contentView.frame = CGRect(x: 0, y: 0, width: widthKeyView, height: heightKeyView)
            contentView.center = CGPoint(x: self.containView.frame.width/2, y: self.containView.frame.height/2)
        }
        else{
            contentView.frame = CGRect(x: contentView.frame.origin.x, y: contentView.frame.origin.y, width: widthKeyView * CGFloat(countKey + 1) + spacingKeyView * CGFloat(countKey), height: heightKeyView)
        }
        containView.addSubview(contentView)
        contentView.addSubview(subView)
        
        UIView.animate(withDuration: 0.7) {
            subView.center = CGPoint(x: self.contentView.frame.width - self.widthKeyView, y: self.contentView.frame.height/2)
        }
        
        UIView.animate(withDuration: 0.7) {
            self.contentView.center = CGPoint(x: self.containView.frame.width/2, y: self.containView.frame.height/2)
        }
        
        subView.translatesAutoresizingMaskIntoConstraints = false
        if countKey == 0{
            firstSubViewConstraint(referencedView: contentView, selectedView: subView, widthKeyView, heightKeyView)
        }
        else{
            let temp = contentView.subviews[countKey - 1]
            temp.translatesAutoresizingMaskIntoConstraints = false
            followingViewConstraint(referencedView: temp, selectedView: subView, widthKeyView, heightKeyView)
        }
    }
    
    func firstSubViewConstraint(referencedView: UIView, selectedView: UIView, _ width: CGFloat, _ height: CGFloat){
        let topConstraint = NSLayoutConstraint(item: selectedView, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1, constant: 0)
        let leftConstraint = NSLayoutConstraint(item: selectedView, attribute: .leading, relatedBy: .equal, toItem: referencedView, attribute: .leading, multiplier: 1, constant: 0)
        let heigtConstraint = NSLayoutConstraint(item: selectedView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: height)
        let widthConstraint = NSLayoutConstraint(item: selectedView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: width)
        contentView.addConstraints([topConstraint, leftConstraint, heigtConstraint, widthConstraint])
    }
    
    func followingViewConstraint(referencedView: UIView, selectedView: UIView, _ height: CGFloat, _ width: CGFloat){
        let topConstraint = NSLayoutConstraint(item: selectedView, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1, constant: 0)
        let leftConstraint = NSLayoutConstraint(item: selectedView, attribute: .leading, relatedBy: .equal, toItem: referencedView, attribute: .trailing, multiplier: 1, constant: spacingKeyView)
        let heigtConstraint = NSLayoutConstraint(item: selectedView, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: height)
        let widthConstraint = NSLayoutConstraint(item: selectedView, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: width)
        contentView.addConstraints([topConstraint, leftConstraint, heigtConstraint, widthConstraint])
    }
    
    func loadKey(key: UIView, value: String){
        if Int(value) != nil{
            let keylabel = UILabel(frame: CGRect(x: 0, y: 0, width: widthKeyView, height: heightKeyView))
            keylabel.text = value
            keylabel.textAlignment = .center
            keylabel.textColor = .white
            keylabel.center = CGPoint(x: key.frame.width/2, y: key.frame.height/2)
            key.addSubview(keylabel)
        }
        else{
            let keyImage = UIImageView(frame: CGRect(x: 0, y: 0, width: widthKeyView, height: heightKeyView))
            keyImage.image = UIImage(named: value)
            keyImage.center = CGPoint(x: key.frame.width/2, y: key.frame.height/2)
            key.addSubview(keyImage)
        }
    }

    func beginGesture(gesture: UIPanGestureRecognizer){
        var cancel = true
        startPoint = gesture.location(in: self.PatternLockCollection)
        if countKey < 4{
            countKey = 0
        }
        else{
            gesture.state = .cancelled
            return
        }
        index_circle = Int((startPoint.y - edgeTop) / distance2Center) * Int(column) +  Int((startPoint.x - edgeLeft) / distance2Center)
        if (index_circle < 0 || index_circle > circleArr.count - 1){
            return
        }
        //kiểm tra tọa độ đầu có thuộc vòng tròn cell không
        if isInsideKey(firstCell: true, centerCell: circleArr[index_circle].centerPoint, point: startPoint){
            //xóa các draw đã vẽ trước đó và reset trackPath mỗi khi bắt đầu lượt vẽ mới
            ResetStatePatternLock()
            TrackPath = []

            startPoint = circleArr[index_circle].centerPoint
            selectedKey(position: startPoint, isRedaw: false)
            if let key = circleArr[index_circle].key.first{
                addKey(value: key.value)
            }

            let item = PasswordItem(positionOfCircleArr: index_circle, key: circleArr[index_circle].key)
            emptyPassword ? password.append(item) : (correctPassword = item.key == password[countKey].key ? true : false)
            countKey += 1
            cancel = false
        }
        //chặn/hủy draw nếu tọa độ bắt đầu không thuộc vòng tròn cell
        if cancel{
            gesture.state = .cancelled
        }
    }
    
    func overLines(startPoint: CGPoint, endPoint: CGPoint) -> Bool{
        var startTemp = startPoint
        var endTemp = CGPoint()
        let temp1 = Int(abs(endPoint.x - startPoint.x)) / Int(distance2Center)
        let temp2 = Int(abs(endPoint.y - startPoint.y)) / Int(distance2Center)
        let temp = temp1 >= temp2 ? temp1 : temp2
        if countKey + temp > maxKey{
            return true
        }
        
        while( !(-1...1).contains(startTemp.x - endPoint.x) || !(-1...1).contains(startTemp.y - endPoint.y)){
            endTemp = startTemp
            
            if !(-1...1).contains(startTemp.x - endPoint.x){
                endTemp.x -= distance2Center * (startPoint.x - endPoint.x) / abs(startPoint.x - endPoint.x)
                index_circle -= 1 * Int((startPoint.x - endPoint.x) / abs(startPoint.x - endPoint.x))
            }
            
            if !(-1...1).contains(startTemp.y - endPoint.y){
                endTemp.y -= distance2Center * (startPoint.y - endPoint.y) / abs(startPoint.y - endPoint.y)
                index_circle -= Int(column) * Int((startPoint.y - endPoint.y) / abs(startPoint.y - endPoint.y))
            }
            
            setupLineWillDraw(startPoint: startTemp, endPoint: endTemp)
            handlePassword(index: index_circle)
            startTemp = endTemp
        }
        return false
    }
    
    func setupLineWillDraw(startPoint: CGPoint, endPoint: CGPoint){
        drawLine(line: &drawedLine, startPoint: startPoint, endPoint: endPoint, color: nil)
        TrackPath.append((startPoint, endPoint))
        selectedKey(position: endPoint, isRedaw: false)
        if let key = circleArr[index_circle].key.first{
            addKey(value: key.value)
        }
        self.countKey += 1
    }
    
    func handlePassword(index: Int){
        let item = PasswordItem(positionOfCircleArr: index_circle, key: circleArr[index].key)
        if emptyPassword{
            password.append(item)
        }
        else{
            //check password
            let nextCorrectPassword = countKey > password.count ? false : (item.key == password[countKey - 1].key ? true : false)
            correctPassword = correctPassword && nextCorrectPassword
        }
    }
    
    func changeGesture(gesture: UIPanGestureRecognizer){
        endPoint = gesture.location(in: self.PatternLockCollection)
        let temppoint = endPoint
        var overKey = false
        let index = Int((endPoint.y - edgeTop) / distance2Center) * Int(column) +  Int((endPoint.x - edgeLeft) / distance2Center)
        if (0...circleArr.count - 1).contains(index) && isInsideKey(firstCell: false, centerCell: circleArr[index].centerPoint, point: endPoint) && circleArr[index].centerPoint != startPoint {
            endPoint = circleArr[index].centerPoint
            drawingLine.removeFromSuperview()

            switch abs(startPoint.x - endPoint.x) - abs(startPoint.y - endPoint.y){
            case abs(startPoint.x - endPoint.x), -abs(startPoint.y - endPoint.y), -1...1 : //draw row, column, cross of square
                overKey = overLines(startPoint: startPoint, endPoint: endPoint)
            default:
                if countKey == maxKey - 1{
                    overKey = true
                    break
                }
                index_circle = index
                setupLineWillDraw(startPoint: startPoint, endPoint: endPoint)
                handlePassword(index: index)
            }
            
            if overKey{
                endPoint = temppoint
            }
            else{
                if countKey == maxKey{
                    gesture.state = .ended
                }
                startPoint = circleArr[index].centerPoint
                return
            }
        }
        //ngược lại tọa độ thay đổi không thuộc vòng tròn thì vẽ đường thẳng thay đổi theo tọa độ
        drawingLine.removeFromSuperview()
        drawLine(line: &drawingLine, startPoint: startPoint, endPoint: endPoint, color: nil)
    }
    
    func endGesture(gesture: UIPanGestureRecognizer){
        drawingLine.removeFromSuperview()
        if countKey == 1{
            ResetStatePatternLock()
        }
        redrawPath(correct: countKey >= minKey ? true : false)
        checkPassword()
    }
    
    func checkPassword(){
        if correctPassword && password.count == countKey {
            self.animateView.transform = .identity
            let vc = storyboard?.instantiateViewController(withIdentifier: "loggedInVC") as! LoggedInViewController
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    func redrawPath(correct: Bool){
        var redawedLine = LineView()
        let color : CGColor = correct == true ? UIColor.green.cgColor : UIColor.red.cgColor
        
        for draw in viewDraw.subviews {
            draw.removeFromSuperview()
        }
        
        for point in TrackPath{
            selectedKey(position: point.0, isRedaw: true)
            drawLine(line: &redawedLine, startPoint: point.0, endPoint: point.1, color: color)
            selectedKey(position: point.1, isRedaw: true)
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            self.imageCircleView.removeFromSuperview()
        }
    }
    
    func ResetStatePatternLock(){
        for view in self.viewDraw.subviews{
            view.removeFromSuperview()
        }
        
        for view in containView.subviews{
            view.removeFromSuperview()
        }
        
        for view in contentView.subviews{
            view.removeFromSuperview()
        }
    }
    
    func resetState(loadView: Bool){
        ResetStatePatternLock()
        self.numberTempRandom = self.keycode
        
        for item in password{
            if let key = item.key.first{
                numberTempRandom.removeValue(forKey: key.key)
            }
        }
        if loadView{
            self.circleArr = []
            DispatchQueue.main.async {
                self.PatternLockCollection.reloadData()
            }
        }
    }
    
    @IBAction func clickResetButton(_ sender: Any) {
        password = []
        resetState(loadView: false)
        emptyPassword = true
        countKey = 0
    }
    
    @IBAction func clickCheckPasswordButton(_ sender: Any) {
        if countKey >= minKey{
            resetState(loadView: false)
            emptyPassword = false
            notchecking = false
            resetButton.isEnabled = false
            resetButton.alpha = 0.5
        }
    }
}
extension PatternLockViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return keycode.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = PatternLockCollection.dequeueReusableCell(withReuseIdentifier: "PatternLockCell", for: indexPath) as! PatternLockCollectionViewCell
        cell.cellBoxImage.image = UIImage(named: "box_passs.png")
        cell.alpha = 0.6
        cell.cellLabel.alpha = 0.4
        cell.cellKeyImage.alpha = 0.4

        //tạo giá trị random trên collection view
        var temp = numberTempRandom.randomElement()
        for item in password{
            if indexPath.row == item.positionOfCircleArr && item.key.first != nil{
                temp = item.key.first
                break
            }
        }
        guard let key = temp else {
            return cell
        }
        if Int(key.value) != nil{
            cell.cellLabel.isHidden = false
            cell.cellKeyImage.isHidden = true
            cell.cellLabel.text = key.value
            cell.cellLabel.font = UIFont.systemFont(ofSize: edge/CGFloat(2))
            cell.cellLabel.textColor = .white
        }
        else {
            cell.cellLabel.isHidden = true
            cell.cellKeyImage.isHidden = false
            if let value = keycode[key.key]{
               cell.cellKeyImage.image = UIImage(named: value)
            }
        }
        numberTempRandom.removeValue(forKey: key.key)
        
        let circle = Circle(key: [key.key: key.value], centerPoint: cell.center)
        circleArr.append(circle)
        return cell
    }
}
