//
//  PatternLockCollectionViewCell.swift
//  patternUnlock
//
//  Created by Admin on 3/18/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class PatternLockCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var cellLabel: UILabel!
    @IBOutlet weak var cellBoxImage: UIImageView!
    @IBOutlet weak var cellKeyImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
}
