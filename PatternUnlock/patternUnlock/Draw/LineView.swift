//
//  PatternView.swift
//  patternUnlock
//
//  Created by Admin on 3/18/19.
//  Copyright © 2019 Admin. All rights reserved.
//

import UIKit

class LineView: UIView {
    
    var startPoint = CGPoint()
    var endPoint = CGPoint()
    var colorLine = UIColor.white.cgColor
    
    func setPoints(fromPoint: CGPoint, toPoint: CGPoint){
        startPoint = fromPoint
        endPoint = toPoint
    }
    
    func setColorLine(color: CGColor){
        colorLine = color
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.init(white: 0.0, alpha: 0.0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        let context = UIGraphicsGetCurrentContext()
        context?.setLineWidth(2)
        context?.setStrokeColor(colorLine)
        context?.move(to: startPoint)
        context?.addLine(to: endPoint)
        
        context?.strokePath()
    }
    
}
